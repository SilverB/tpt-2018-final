var config = require('../../nightwatch.conf.js');

module.exports = {
    'example ekspress': function(browser) {
        browser
            .url('https://www.tptlive.ee/')
            .waitForElementVisible('#main-menu-wrapper', 1000)
            .saveScreenshot(config.imgpath(browser) + 'tptlive.png')
            .click('#menu-item-1313')
            .pause(1000)
            .saveScreenshot(config.imgpath(browser) + 'tptlivetunniplaan.png')
            .useXpath()
            .click("//a[text()='TA-17E']")
            .useCss()
            .waitForElementVisible('body')
            .saveScreenshot(config.imgpath(browser) + 'tptliveTA17E.png')
            .pause(3000)
            .end();
    }
};